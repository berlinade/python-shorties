import numpy as np

from scipy.sparse import csr_matrix

import yaml  # pip install pyyaml

import pickle


np_arr_tag: str = '!NumpyArray'
sp_csr_tag: str = '!ScipyCSR'


def repres_np_array(dumper: yaml.BaseDumper, data: np.ndarray):
    _tmp: list = data.tolist()
    return dumper.represent_sequence(np_arr_tag, _tmp)


def constructor_np_array(loader: yaml.BaseLoader, node):
    return np.array(loader.construct_sequence(node, deep = True))


def repres_sp_csr(dumper: yaml.BaseDumper, indata: csr_matrix):
    return dumper.represent_mapping(sp_csr_tag, dict(data = indata.data.tolist(),
                                                     indices = indata.indices.tolist(),
                                                     indptr = indata.indptr.tolist(),
                                                     shape = list(indata.shape)))


def constructor_sp_csr(loader: yaml.BaseLoader, node):
    sub_dict = loader.construct_mapping(node, True)
    return csr_matrix((sub_dict['data'], sub_dict['indices'], sub_dict['indptr']), shape = sub_dict['shape'])


def main():
    """ ... """

    ''' dense array '''
    arr_data: list[list[float]] = [[1.1, 2.02], [3.003, 4.0004]]
    arr = np.array(arr_data)
    print(arr)

    ''' sparse matrix '''
    data_csr = np.array([1.9, 3.009, 2.09, 4.0009])
    indices_csr = np.array([0, 2, 1, 1])
    indptr_csr = np.array([0, 1, 3, 3, 4])
    shape_csr = (4, 4)
    mat_csr = csr_matrix((data_csr, indices_csr, indptr_csr), shape = shape_csr)
    print(mat_csr.toarray())

    yaml_dict: dict = dict(arr = arr, arr_data = arr_data, sp_arr = mat_csr)

    # yaml.add_representer(np.ndarray, representer = repres_np_array, Dumper = yaml.Dumper)
    yaml.add_representer(np.ndarray, representer = repres_np_array, Dumper = yaml.SafeDumper)

    # yaml.add_representer(csr_matrix, representer = repres_sp_csr, Dumper = yaml.Dumper)
    yaml.add_representer(csr_matrix, representer = repres_sp_csr, Dumper = yaml.SafeDumper)

    # yaml.add_constructor(np_arr_tag, constructor = constructor_np_array, Loader = yaml.Loader)
    yaml.add_constructor(np_arr_tag, constructor = constructor_np_array, Loader = yaml.SafeLoader)

    # yaml.add_constructor(sp_csr_tag, constructor_sp_csr, Loader = yaml.Loader)
    yaml.add_constructor(sp_csr_tag, constructor_sp_csr, Loader = yaml.SafeLoader)

    with open('content.safe.yaml', mode = 'w') as out_stream:
        yaml.dump(yaml_dict, out_stream, Dumper = yaml.SafeDumper, default_flow_style = None)

    with open('content.safe.yaml', mode = 'r') as in_stream:
        barfoo = yaml.load(in_stream, Loader = yaml.SafeLoader)

    print('barfoo')
    print(barfoo)
    print(barfoo['sp_arr'].toarray())


if __name__ == '__main__': main()
