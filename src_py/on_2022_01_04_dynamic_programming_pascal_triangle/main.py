from typing import TypeAlias, Optional, Protocol


Mem: TypeAlias = dict[tuple[int, int], int]


class BinomFun(Protocol):
    def __call__(self, n: int, k: int, get_mem: Optional[bool] = False) -> int | Mem: ...


def memorization(binom_fun: BinomFun) -> BinomFun:
    mem: Mem = dict()

    def wrapped_binom(n: int, k: int, get_mem: Optional[bool] = False) -> int | Mem:
        if get_mem: return mem
        k = min(k, n - k)
        arg: tuple[int, int] = (n, k)
        if arg not in mem: mem[arg] = binom_fun(*arg)
        return mem[arg]
    return wrapped_binom


@memorization
def binom(n: int, k: int, _: Optional[bool] = False) -> int:
    assert n >= 0
    assert k >= 0
    assert k <= n
    if n == 0: return 1
    if (k == 0) or (k == n): return 1
    return binom(n - 1, k - 1) + binom(n - 1, k)


def main():
    for n in range(25):
        for k in range(n + 1):
            print(binom(n, k), end = ' ')
        print('')

    print(binom(0, 0, True))


if __name__ == '__main__': main()
