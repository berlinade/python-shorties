# Python Shorties

__author information__ : [codima](https://www.youtube.com/channel/UCwnthITQqkWgaHnz82U7WsA) (coding-mathmatics) on youtube

__german__ : Hallo, hier findet ihr diverse kleinere python Skripte die ich für/wegen/während oder in Folge meiner Videos verfasst habe.

__informelle Lizenzinfo (german)__ : Dieses gesamte Repository steht unter GPL Lizenz (siehe LICENSE). Zwecks Akkreditierung könnt ihr codima als den Autor dieses Pakets benennen.

__informal info on license (english)__ : This repository is licensed under GPL (see LICENSE). You can refer to the author of this package as codima.
